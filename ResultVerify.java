import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;             
import java.util.Base64;

// Java program to calculate SHA hash value
class GFG2 {

    public static byte[] getSHA(String input) throws NoSuchAlgorithmException
    {
        // Static getInstance method is called with hashing SHA
        MessageDigest md = MessageDigest.getInstance("SHA-256");
 
        // digest() method called
        // to calculate message digest of an input
        // and return array of byte
        return md.digest(input.getBytes(StandardCharsets.UTF_8));
    }

    public static String toHexString(byte[] hash)
    {
        // Convert byte array into signum representation
        BigInteger number = new BigInteger(1, hash);
 
        // Convert message digest into hex value
        StringBuilder hexString = new StringBuilder(number.toString(16));
 
        // Pad with leading zeros
        while (hexString.length() < 64)
        {
            hexString.insert(0, '0');
        }

        return hexString.toString().toUpperCase();
    }
 

    // Driver code
    public static void main(String args[])
    {
        try
        {
            String result = "eyJhbW91bnQiOjg3ODA0LCJhbW91bnRfZm9yZWlnbiI6bnVsbCwiYW1vdW50X29yaWdpbmFsIjpudWxsLCJhbW91bnRfcmVxdWVzdCI6ODc4MDQsImJhbmsiOm51bGwsImNhcmRfYnJhbmQiOiJTTUwiLCJjYXJkX2luZm8iOnsiY2FyZF9uYW1lIjoiTkdVWUVOIFZBTiBBIiwiaGFzaF9jYXJkIjoiNTUwNGI5ODA4Y2RhYmU3YmRkMWQ4NWI0MDM5MjBiMjUiLCJjYXJkX2JyYW5kIjoiU01MIiwiY2FyZF9udW1iZXIiOiI5NzA0MDB4eHh4eHgwMDE4In0sImNyZWF0ZWRfYXQiOiIyMDIyLTA5LTA3VDAyOjE1OjA1LjAwMDAwMFoiLCJjdXJyZW5jeSI6IlZORCIsImRlc2NyaXB0aW9uIjoiTcO0IHThuqMgZ2lhbyBk4buLY2giLCJlcnJvcl9jb2RlIjoiMDAwIiwiZXhjX3JhdGUiOm51bGwsImZhaWx1cmVfcmVhc29uIjpudWxsLCJmb3JlaWduX2N1cnJlbmN5IjpudWxsLCJpbnZvaWNlX25vIjoiMTY2MzE5NDk2OCIsImxhbmciOm51bGwsIm1ldGhvZCI6IkFUTV9DQVJEIiwicGF5bWVudF9ubyI6MzEwMzk2MDM3MTI3LCJzdGF0dXMiOjUsInRlbm9yIjpudWxsfQ";
            String checksum = "4651984B872B65017FFDDD5E61212AC2E8154052B2C69E322EDF7AE814CAC92F";
            String checksum_key = "ikrvIkEssytXcI6hV8r6Hxv3X5xB8iwK";

            String s1 = "CheckSum received";
            System.out.println("\n" + s1 + " : " + checksum);
 
            String checksum_gen = toHexString(getSHA(result + checksum_key));
            String s2 = "CheckSum generated ";
            System.out.println("\n" + s2 + " : " + checksum_gen);
 
            System.out.println("\n" + checksum.equals(checksum_gen));  
            
	    // Decode base64 data
            byte[] decodedBytes = Base64.getDecoder().decode(result);
            String decodedString = new String(decodedBytes);
            System.out.println(decodedString); 
        }

        // For specifying wrong message digest algorithms
        catch (NoSuchAlgorithmException e) {
            System.out.println("Exception thrown for incorrect algorithm: " + e);
        }
    }
}
