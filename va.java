import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.net.URL;
import java.net.HttpURLConnection;
import java.util.*;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;

public class NinePay {

	private static final String MERCHANT_KEY = "";

	private static final String MERCHANT_SECRET_KEY = "";
	private static final String END_POINT = "https://sand-payment.9pay.vn";

	public static void main(String[] args) throws Exception {
		String time = String.valueOf(System.currentTimeMillis()/1000);

		Map<String, String> map = new TreeMap<String, String>();
		map.put("request_id", "123218938912838");
		map.put("uid", "11612");
		map.put("uname", "daonam");
		map.put("bank_code", "BIDV");
		String queryHttp = http_build_query(map);
		String mesage = buildMessage(queryHttp, time);
		String signature = signature(mesage,MERCHANT_SECRET_KEY);	

        // Start to call

        StringBuilder postData = new StringBuilder();
        for (Map.Entry<String,String> param : map.entrySet()) {
            if (postData.length() != 0) postData.append('&');
            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
        }
        byte[] postDataBytes = postData.toString().getBytes("UTF-8");        
        

        URL url = new URL(END_POINT + "/va/create");
        HttpURLConnection http = (HttpURLConnection)url.openConnection();
        http.setRequestProperty("Date", time);
        http.setRequestProperty("Authorization", "Signature Algorithm=HS256,Credential=" + MERCHANT_KEY + ",SignedHeaders=,Signature=" + signature);
        http.setRequestMethod("POST");
        http.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
        http.setDoOutput(true);
        http.getOutputStream().write(postDataBytes);


        System.out.println(http.getResponseCode() + " " + http.getResponseMessage());

        BufferedReader in = new BufferedReader(new InputStreamReader(http.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
        System.out.println(response.toString());
        http.disconnect();

	}

	public static String buildMessage(String queryHttp, String time) {
		StringBuffer sb = new StringBuffer();
		sb.append("POST");
		sb.append("\n");
		sb.append(END_POINT + "/va/create");
		sb.append("\n");
		sb.append(time);
		sb.append("\n");		
	    sb.append(queryHttp);		
		return sb.toString();
	}

	public static String signature(String queryHttp, String secretKey) {
		try {
			Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
			SecretKeySpec secret_key = new SecretKeySpec(secretKey.getBytes(), "HmacSHA256");
			sha256_HMAC.init(secret_key);
			byte[] hmac = sha256_HMAC.doFinal(queryHttp.getBytes());
			return Base64.getEncoder().encodeToString(hmac);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String http_build_query(Map<String, String> array) {
		String reString = "";
		Iterator it = array.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, String> entry = (Map.Entry) it.next();
			String key = entry.getKey();
			String value = entry.getValue();
			reString += key + "=" + value + "&";
		}
		reString = reString.substring(0, reString.length() - 1);
		reString = java.net.URLEncoder.encode(reString);
		reString = reString.replace("%3D", "=").replace("%26", "&");
		return reString;
	}

}
