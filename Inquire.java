import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.net.URL;
import java.net.HttpURLConnection;
import java.util.*;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;

public class NinePay {

	private static final String MERCHANT_KEY = "5scxvm";

	private static final String MERCHANT_SECRET_KEY = "OcSiXN5QrTz1n2dATbO1ZjRkRT8Ii5Yzckr";

	private static final String END_POINT = "https://sand-payment.9pay.vn";

	public static void main(String[] args) throws Exception {
		String time = String.valueOf(System.currentTimeMillis()/1000);
		String invoiceNo = "A.FS.18496";
		String mesage = buildMessage(time, invoiceNo);
		String signature = signature(mesage,MERCHANT_SECRET_KEY);	

        URL url = new URL(END_POINT + "/v2/payments/" + invoiceNo + "/inquire");
        HttpURLConnection http = (HttpURLConnection)url.openConnection();
        http.setRequestProperty("Date", time);
        http.setRequestProperty("Authorization", "Signature Algorithm=HS256,Credential=" + MERCHANT_KEY + ",SignedHeaders=,Signature=" + signature);
        
        System.out.println(http.getResponseCode() + " " + http.getResponseMessage());

        BufferedReader in = new BufferedReader(new InputStreamReader(http.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
        System.out.println(response.toString());
        http.disconnect();

	}

	public static String buildMessage(String time, String invoiceNo) {
		StringBuffer sb = new StringBuffer();
		sb.append("GET");
		sb.append("\n");
		sb.append(END_POINT + "/v2/payments/" + invoiceNo + "/inquire");
		sb.append("\n");
		sb.append(time);
		return sb.toString();
	}

	public static String signature(String queryHttp, String secretKey) {
		try {
			Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
			SecretKeySpec secret_key = new SecretKeySpec(secretKey.getBytes(), "HmacSHA256");
			sha256_HMAC.init(secret_key);
			byte[] hmac = sha256_HMAC.doFinal(queryHttp.getBytes());
			return Base64.getEncoder().encodeToString(hmac);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}

